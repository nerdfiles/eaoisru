###
@fileOverview ./evolve.coffee
@author nerdfiles <hello@nerdfiles.net>
@created Fri Apr 15 12:05:54 CDT 2016
@description
Possible-world program for identity in cyberspace to very dense halting states in 
polynomial time bounded-error deterministic homomorphic spaces.
###

