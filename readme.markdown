# Eaoisru

Conceptually hygienic mereological multi-modal truth-procedure construction world 
builder toolkit, as we must be able to distinguish Machines with Identity in a P2P 
networking layer that is universal system that sends/receives via Unitary Reference 
Architecture.

More generally, Eaoisru is an S5-like logic with Kripke Semantics intended for a 
logical layer of a distributed ledger technology combined with configurable and dynamic
DNS routing table that stores a Distributed File Microservice sharding pattern: [White Paper][filemicro].

## Operator Definitions

    UR-URI Concept             Symbol    Canonical                              Eaoisruian Concept                Eaoisruian Operator
    Uniqueness                 $         /aka/$President                        Possibility                       ◇
    Persistence                _         /aka/_endurance                        Necessity                         □
    Universality               -         /ui/btn--payment or /ui/p-text         Names                             Millianism
    Scalability                .         /ui/WebPage.breadcrumb                 Conjunctive Inclusion             & or ^
    Evolvability               +         /semver/1.0.0+38293/packageName        Disjunctive Inclusion             + or v
    Opacity                    * (or **) /A/produces/A/ or /A/**/C              Arrow (Material Entailment)       ⊃ or >
    No Side-effects            '         /savedRecord' (H Factor: LT)           Negation                          -
    Global uniqueness          ()        /style/(idAttr)                        Universal Quantifier              () or ∀
    Optional sameness          (())      /style/((className))                   Existential Quantifier            ∃ (quantifier exchange?) or `P(query=None)` (hopefully our API can be this simple)
    Fully Non-dereferenceable  !         /document/!main                        definition                        ≔ or ≡
    Infinite series expansion  ,         /itemA,itemB,itemC                     Induction|Grouping                (weak,minor,strong,etc) premises distinctions and weighting?

Eaoisru contains a propositional calculus as well. Ultimately Eaoisru is a 
development of Distributed Order Execution. Orders are Propositional Generics 
that are inserted into a process digest-cycle that implicates N-qubit registers.

Practically, Deflationary Orders of Trust are configurations of DOE over certain 
members of a distributed network topology that supports quantum push, field broadcasting, 
and various other advanced communication techniques to record and quantize 
error material conditions for local collapse/compute vector-cadence normalizations.

## Possible-world Programs for Identity in Cyberspace

The fundamental conceivability of such a URA presumes that a quantum receiver need 
only operate on the transmitted states and not necessarily measure those states. A 
quantum receiver in this case might be a handheld mobile device or a desktop 
computer.

Such “programs” will have the chief business of preserving quantum communications as 
a networking protocol layer which underpins the Periodic Table of Information. A potential 
set of programs might include:

1. Quantum Memory Modules
2. Quantum Router
3. Quantum Key Distribution (this is actually lattice-based QFT; see Microsoft)
4. Quantum Teleportation
5. Quantum Entanglement

### Key Terms

    TX   : Quantum Transmitter
    RX   : Quantum Receiver
    SDQC : Software-defined Quantum Communication
    QLD  : Quantum Light Detector
    QLS  : Quantum Light Source
    HDD  : Hardware Device Driver
    CPU  : Computer Processing Unit

### SDQC Structure

    TX                                     RX
    CPU ------------- W/LAN ------------- CPU
     |                                     |
    HDD                                   HDD
     |                                     |
    QLS -------- Quantum Channel -------- QLD

## Features

### States

Qubits store hex-encoded state-transition graphs (Sebastion calls these 
"images") of keys.

### Time evolution

Efficient state snapshots encode Closed Time-like Curves. Data at a distance, 
data at rest, data in transit, etc.

### Restriction

1. Constraints expressed as Boolean formula encoded into quantum algorithm. 
   Uses algorithm where partial measurement is used in the amplitude 
   amplification process instead of the usual unitary amplitude amplification 
   techniques that use iterative calls to an oracle to amplify the required 
   solution.

### Observable

Count-as-one.

### Instrument

### Separable channel

### Pure channel

Initial multiplicity.

### Teleportation

## Conceptual Spheres

### Amorous

### Political

### Biological

### Aesthetical

## Conceptual Dimensions

### Explication

### Aggregation

### Organization

### Integration

### Synthesis

### Resolution

### Unification

[filemicro]: https://www.researchgate.net/publication/285288610_File_Microservices_and_Federated_Available_and_Reliable_Reporting_Storage_for_a_Trustless_Co-located_Environment
